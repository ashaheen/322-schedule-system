/**
 * Time Class
 * Custom datatype for use in scheduling system. Holds information about event time. 
 */
public class Time {

	private int timezone;
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;

	public Time() {
		/**
		 * To be implemented
		 */
	}
	
	/**
	 * Setter/getter methods
	 */
	
	public void setTimezone(int timezone){
		this.timezone = timezone;
	}
	
	public void setYear(int year){
		this.year = year;
	}
	
	public void setMonth(int month){
		this.month = month;
	}
	
	public void setDay(int day){
		this.day = day;
	}
	
	public void setHour(int hour){
		this.hour = hour;
	}
	
	public void setMinute(int minute){
		this.minute = minute;
	}
	
	public int getTimezone(){
		return timezone;
	}
	
	public int getYear(){
		return year;
	}
	
	public int getMonth(){
		return month;
	}
	
	public int getDay(){
		return day;
	}
	
	public int getHour (){
        return hour;
	}
	
	public int getMinute (){
        return minute;
	}
		
}

