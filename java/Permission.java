/**
 * Permission Class
 * Enables user access to schedule instances. Permission class is a powerful access layer. 
 * It enables the schedules owners to manage their own schedules, and it allows for the ability
 * to share access. A user can provide another user the permission to view/manage their schedules.
 */
public class Permission {
	private int userID, scheduleID;
	private boolean isOwner, canView, canManage;
	
	public Permission() {
		/**
		 * To be implemented
		 */
	}
	
	/**
	 * Setter/getter methods
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	public void setScheduleID(int scheduleID) {
		this.scheduleID = scheduleID;
	}
	
	public void setOwner(boolean isOwner) {
		this.isOwner = isOwner;
	}
	
	public void setCanView(boolean canView) {
		this.canView = canView;
	}
	
	public void setCanManage(boolean canManage) {
		this.canManage = canManage;
	}
	
	public int getUserID() {
		return userID;
	}
	
	public int getScheduleID() {
		return scheduleID;
	}
	
	public boolean isOwner() {
		return isOwner;
	}
	
	public boolean canManage() {
		return canManage;
	}
	
	public boolean canView() {
		return canView;
	}
}
