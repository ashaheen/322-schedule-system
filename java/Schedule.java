/**
 * Schedule Class
 * Enables user to interact with the schedule by retrieving and displaying events. It also enables
 * actions such as 'view', 'add', 'modify', 'delete' events.
 */
public class Schedule {

	private int scheduleID;
	private String title;
	private String description;
	
	public Schedule() {
		/**
		 * To be implemented
		 */
	}
	
	public Event[] retrieveEvents() {
		/**
		 * To be implemented
		 */
		return null;
	}
	
	public Event[] searchEvents() {
		/**
		 * To be implemented
		 */
		return null;
	}
	
	/**
	 * Setter/getter methods
	 */
	
	public void setScheduleID(int scheduleID){
		this.scheduleID = scheduleID;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public int getScheduleID(){
		return scheduleID;
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getDescription(){
		return description;
	}
}