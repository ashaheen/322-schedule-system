/**
 * Location Class
 * A data type used in the scheduling system to maintain the information about a certain 
 * geographic location.
 */
public class Location {

	private String address;
	private String postalCode;
	private String city;
	private String stateOrProvince;
	private String country;
	
	public Location() {
		/**
		 * To be implemented
		 */
	}
	
	/**
	 * Setter/getter methods
	 */
	
	public void setAddress(String address){
		this.address = address;
	}
	
	public void setPostalCode(String postalCode){
		this.postalCode = postalCode;
	}
	
	public void setCity(String city){
		this.city = city;
	}
	
	public void setStateOrProvince(String stateOrProvince){
		this.stateOrProvince = stateOrProvince;
	}
	public void setCountry(String country){
		this.country = country;
	}
	
	public String getAddress(){
		return address;
	}
	public String getPostalCode(){
		return postalCode;
	}
	public String getCity(){
		return city;
	}
	public String getStateOrProvince(){
		return stateOrProvince;
	}
	public String getCountry(){
		return country;
	}
	
}
