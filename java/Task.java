/**
 * Task Class
 * Subclass of Event, it represents a day to day task that the user needs to perfrom. It is
 * different form Meeting in the way that tasks act like a to-do list that the user needs to
 * complete. 
 */
public class Task extends Event{

	private int department;
	private String project;
	private boolean isComplete;

	public Task() {
		/**
		 * To be implemented
		 */
	}
	/**
	 * Setter/getter methods
	 */
	
	public void setDepartment(int department){
		this.department = department;
	}
	
	public void setProject(String project){
		this.project = project;
	}
	
	public void setIsComplete(boolean isComplete){
		this.isComplete = isComplete;
	}
	
	public int getDepartment(){
		return department;
	}
	
	public String getProject(){
		return project;
	}
	
	public boolean getIsComplete(){
		return isComplete;
	}
}

