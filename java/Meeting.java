/**
 * Permission Class
 * Subclass of Event. This is the primary object that represents the user meetings.
 * It can be extended to support a wide variety of features.
 */
public class Meeting extends Event {
	private String type;
	
	public Meeting() {
		/**
		 * To be implemented
		 */
	}
	
	/**
	 * Setter/getter methods
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
}
