
public class User {
	private int userID, timezone;
	private String firstName, lastName, email, userName, password;
	private Location location;
	
	public User() {
		/**
		 * To be implemented
		 */
	}
	
	/**
	 * Setter/getter methods
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public void setTimezone(int timezone) {
		this.timezone = timezone;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setUsername(String userName) {
		this.userName = userName;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public int getUserID(){
		return userID;
	}
	public int getTimezone(){
		return timezone;
	}
	public String getFirstName(){
		return firstName;
	}
	public String getLastName(){
		return lastName;
	}
	public String getEmail(){
		return email;
	}
	public String getUserName(){
		return userName;
	}
	public String getPassword(){
		return password;
	}
	public Location getLocation(){
		return location;
	}
}
