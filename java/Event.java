/**
 * Event Abstract Class
 * Base class that represents the meetings and tasks that a user can put on their 
 * schedule. Every event is associated with a certain schedule.
 */
public abstract class Event {

	private String name;
	private int scheduleID;
	private String description;
	private Location location;
	private Time startTime;
	private Time endTime;
	private int eventID;
	
	public Event() {
		/**
		 * To be implemented
		 */
	}
	
	/**
	 * Setter/getter methods
	 */
	public void setName(String name){
		this.name = name;
	}
	
	public void setScheduleID(int scheduleID){
		this.scheduleID = scheduleID;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public void setLocation(Location location){
		this.location = location;
	}
	
	public void setStartTime(Time startTime){
		this.startTime = startTime;
	}
	
	public void setEndTime(Time endTime){
		this.endTime = endTime;
	}
	
	public void setEventID(int eventID){
		this.eventID = eventID;
	}
	
	public String getname(){
		return name;
	}
	
	public int getScheduleID(){
		return scheduleID;
	}
	
	public String getDescription(){
		return description;
	}
	
	public Location getLocation(){
		return location;
	}
	
	public Time getStartTime(){
		return startTime;
	}
	
	public Time getEndTime(){
		return endTime;
	}
	
	public int getEventID(){
		return eventID;
	}
}

